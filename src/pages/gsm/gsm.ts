import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

@IonicPage()
@Component({
  selector: 'page-gsm',
  templateUrl: 'gsm.html',
})
export class GsmPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public bluetoothSerial:BluetoothSerial) {
  }

  sendMessage(){
    // console.log("Sending message");

    var data = new ArrayBuffer(4);
    data[0] = 'hola mond';
    data[1] = 123;
    data[2] = " mundo";
    data[3] = 456;

    this.bluetoothSerial.write(data);

    
    //this.bluetoothSerial.write( [186, "assa", 222] );
  }
}
