import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PuertaPage } from './puerta';

@NgModule({
  declarations: [
    PuertaPage,
  ],
  imports: [
    IonicPageModule.forChild(PuertaPage),
  ],
})
export class PuertaPageModule {}
