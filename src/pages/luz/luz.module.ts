import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LuzPage } from './luz';

@NgModule({
  declarations: [
    LuzPage,
  ],
  imports: [
    IonicPageModule.forChild(LuzPage),
  ],
})
export class LuzPageModule {}
