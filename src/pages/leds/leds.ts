import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';  

@IonicPage()
@Component({
  selector: 'page-leds',
  templateUrl: 'leds.html',
})
export class LedsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
              private bluetoothSerial: BluetoothSerial, public toastCtrl: ToastController) {
  }

  sendWord( word ){

    console.log( word );
    this.bluetoothSerial.write( word );

  }  

  getInfo( ){

    let alert = this.alertCtrl.create({
      title: 'Info sensor!',
      subTitle: 'La temperatura actual es: '+
                'La humerdad:',
      buttons: ['OK']
    });

    this.bluetoothSerial.write( 'i' );
    this.bluetoothSerial.read().then( data => {

      let toast = this.toastCtrl.create({
        message: data,
        duration: 3000
      });
      toast.present();
      
    });
    
  } 

}
