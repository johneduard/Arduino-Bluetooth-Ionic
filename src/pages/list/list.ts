import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { 
  AudioPage,
  GsmPage,
  LedsPage,
  LuzPage,
  SensoresPage
 } from '../index.pages';
import { ReportProvider } from '../../providers/report/report';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  icons: string[];
  items: Array<{title: string, component: any, icon: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public _rc: ReportProvider) {
    
    this.items = [
      { title: 'Audio', component: AudioPage, icon: 'megaphone' },
      { title: 'Bluetooth transmission', component: LedsPage, icon: 'bluetooth' },
      { title: 'GSM', component: GsmPage, icon: 'wifi' },
      { title: 'Luz', component: LuzPage, icon: 'bulb' },
      { title: 'Sensores', component: SensoresPage, icon: 'stopwatch' }
    ];

    // this._rc.reportLists().then();
  }

  itemTapped(event, item, component) {
    this.navCtrl.push( component, {
      item: item
    });
  }
}
