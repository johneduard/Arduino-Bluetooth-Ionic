import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';


@IonicPage()
@Component({
  selector: 'page-sensores',
  templateUrl: 'sensores.html',
})
export class SensoresPage {

  humedad: number = 20;
  temperatura: number = 20;

  constructor(public navCtrl: NavController, public navParams: NavParams,private bluetoothSerial: BluetoothSerial,
            public toastCtrl: ToastController) {
  }

  getData(){

    this.bluetoothSerial.write( 'i' );
    this.bluetoothSerial.read().then( data => {

      let toast = this.toastCtrl.create({
        message: data,
        duration: 3000
      });
      toast.present();
      console.log( data );
      
    });
  }

  
}
