import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MediaCapture } from '@ionic-native/media-capture';
import { Media, MediaObject } from '@ionic-native/media';
import { File } from "@ionic-native/file";

const MEDIA_FILE_KEY = 'mediaFiles';

@IonicPage()
@Component({
  selector: 'page-audio',
  templateUrl: 'audio.html',
})
export class AudioPage {
  mediaFiles = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
              public mediaCapture: MediaCapture, public media: Media, public file: File) {
  }

  ionViewDidLoad() {
    this.storage.get(MEDIA_FILE_KEY).then(res =>{
      this.mediaFiles = JSON.parse(res) || [];
    });
  }

  captureAudio(){
    this.mediaCapture.captureAudio().then( res =>{
      this.storageMediaFiles(res);
    });
  }

  playFile( myFile ){
    //if( myFile.name.indexOf('.wav') > -1 ){
      const audioFile: MediaObject = this.media.create(myFile.localURL);
      audioFile.play();
    //}
  }

  storageMediaFiles(files){
    this.storage.get(MEDIA_FILE_KEY).then( res =>{
      if( res ){
        let arr = JSON.stringify(res);
        arr = arr.concat(files);
        this.storage.set(MEDIA_FILE_KEY, JSON.stringify(arr));
      }else{
        this.storage.set(MEDIA_FILE_KEY, JSON.stringify(files));
      }
      this.mediaFiles = this.mediaFiles.concat(files);
    });
  }

}
